package com.XtraordinairDEV.batteryhomebutton;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.text.util.Linkify;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

public class PreferencesActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {
	public static final String INTENT_SETTINGS_CHANGED = "com.XtraordinairDEV.batteryhomebutton.SETTINGS_CHANGED";
	private static final String URL_MohammadAG_MODULES = "http://repo.xposed.info/users/mohammadag";
	private static final String URL_MohammadAG_APPS = "market://search?q=pub:Mohammad Abu-Garbeyyeh";
	private static final String URL_Xtraordinair_APPS = "market://search?q=pub:Xtraordinair";

	@SuppressLint("WorldReadableFiles")
	@SuppressWarnings("deprecation")
	@Override
	public SharedPreferences getSharedPreferences(String name, int mode) {
		//Gets name from Gradle application name
		fixFolderPermissionsAsync();
		return super.getSharedPreferences(name, Context.MODE_WORLD_READABLE);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.prefs);

		initMohammadAGPreference();
		initXtraordinairPreference();
		initColorPickerPreference();
		initBHIPreference();
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onResume() {
		super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onPause() {
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
		super.onPause();
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences arg0, String arg1) {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				sendBroadcast(new Intent(INTENT_SETTINGS_CHANGED));
			}
		}, 1000);
	}

	@SuppressWarnings("deprecation")
	private void initMohammadAGPreference() {
		Preference mohammadAGPreference = findPreference("MohammadAGKey");
		mohammadAGPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				AlertDialog.Builder builder = new AlertDialog.Builder(PreferencesActivity.this);
				builder.setTitle("")
				.setItems(R.array.MohammadAG_apps, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Uri uri = null;
						Intent intent = new Intent(Intent.ACTION_VIEW);
						switch (which) {
						case 0:
							uri = Uri.parse(URL_MohammadAG_APPS);
							intent.setPackage("com.android.vending");
							break;
						case 1:
							uri = Uri.parse(URL_MohammadAG_MODULES);
							break;
						}
						try {
							startActivity(intent.setData(uri));
						} catch (ActivityNotFoundException e) {
							Toast.makeText(PreferencesActivity.this, "Play Store not found", Toast.LENGTH_SHORT).show();
						}
					}
				});
				builder.create().show();
				return false;
			}
		});
	}

	private void initXtraordinairPreference() {
		Preference preference = findPreference("XtraordinairKey");
		preference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				AlertDialog.Builder builder = new AlertDialog.Builder(PreferencesActivity.this);
				builder.setTitle("")
						.setItems(R.array.Xtraordinair_apps, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								Uri uri = null;
								Intent intent = new Intent(Intent.ACTION_VIEW);
								switch (which) {
									case 0:
										uri = Uri.parse(URL_Xtraordinair_APPS);
										intent.setPackage("com.android.vending");
										break;
								}
								try {
									startActivity(intent.setData(uri));
								} catch (ActivityNotFoundException e) {
									Toast.makeText(PreferencesActivity.this, "Play Store not found", Toast.LENGTH_SHORT).show();
								}
							}
						});
				builder.create().show();
				return false;
			}
		});
	}

	private void initColorPickerPreference() {
		Preference colorPickerPreference = findPreference("color_picker_license");
		colorPickerPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				AlertDialog.Builder builder = new AlertDialog.Builder(PreferencesActivity.this);
				TextView tv = getLicenseTextView(R.string.color_picker_license1);
				builder.setTitle(R.string.color_picker_title)
						.setView(tv)
						.setPositiveButton("OK",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						//Do nothing, just let dialog close
					}
				});
				builder.create().show();
				return false;
			}
		});
	}

	private void initBHIPreference() {
		Preference colorPickerPreference = findPreference("battery_home_icon_license");
		colorPickerPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				AlertDialog.Builder builder = new AlertDialog.Builder(PreferencesActivity.this);
				TextView tv = getLicenseTextView(R.string.battery_home_icon_license1);
				builder.setTitle(R.string.battery_home_icon_title)
						.setView(tv)
						.setPositiveButton("OK",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								//Do nothing, just let dialog close
							}
						});
				builder.create().show();
				return false;
			}
		});
	}

	private TextView getLicenseTextView(int resId){
		final int WEB_LINKS = 1;
		TextView tv = new TextView(PreferencesActivity.this);
		tv.setHorizontallyScrolling(false);
		tv.setText(resId);
		Linkify.addLinks(tv, WEB_LINKS);

		return tv;
	}

	private TextView getLicenseTextView(String licenseText){
		final int WEB_LINKS = 1;
		TextView tv = new TextView(PreferencesActivity.this);
		tv.setHorizontallyScrolling(false);
		tv.setText(licenseText);
		Linkify.addLinks(tv, WEB_LINKS);

		return tv;
	}

	public void fixFolderPermissionsAsync() {
		 AsyncTask.execute(new Runnable() {
			 @Override
			 public void run() {
				 PreferencesActivity.this.getFilesDir().setExecutable(true, false);
				 PreferencesActivity.this.getFilesDir().setReadable(true, false);
				 File sharedPrefsFolder = new File(PreferencesActivity.this.getFilesDir().getAbsolutePath()
						 + "/../shared_prefs");
				 sharedPrefsFolder.setExecutable(true, false);
				 sharedPrefsFolder.setReadable(true, false);
			 }
		 });
	}
}