package com.XtraordinairDEV.batteryhomebutton;

import android.widget.ImageView;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * Created by Steph on 6/9/2016.
 */
public class XposedLockMod implements IXposedHookLoadPackage{
    private final String SYSTEMUI = "com.android.systemui";
    private final String SYSTEMUI_STATUSBAR_PHONE = SYSTEMUI + ".statusbar.phone";

    protected BatteryDrawableLockscreen mBatteryDrawable;
    protected XSharedPreferences mPrefs
            = new XSharedPreferences("com.XtraordinairDEV.batteryhomebutton");

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        if (!loadPackageParam.packageName.equals(SYSTEMUI))
            return;

        if(mPrefs.getBoolean("lockscreen_enable", true)) {
            Class<?> KeyguardBottomAreaView =
                    XposedHelpers.findClass(SYSTEMUI_STATUSBAR_PHONE + ".KeyguardBottomAreaView",
                            loadPackageParam.classLoader);

            XC_MethodHook hookLock = new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    ImageView imageView = (ImageView) XposedHelpers.callMethod(param.thisObject, "getLockIcon");
                    replaceHomeIcon(imageView);

                }
            };
            XposedHelpers.findAndHookMethod(KeyguardBottomAreaView, "onFinishInflate", hookLock);
        }
    }

    private void replaceHomeIcon(ImageView imageView){
        if(mBatteryDrawable == null)
            mBatteryDrawable = BatteryDrawableLockscreen.newInstance(imageView, BatteryDrawableLockscreen.HIDE_PERCENTAGE);
        imageView.setBackground(mBatteryDrawable);
        mBatteryDrawable.setView(imageView);
        ImageViewHelper imageViewHelper = new ImageViewHelper(imageView, mBatteryDrawable);
        imageViewHelper.setReceiver();
        imageViewHelper.onWindowEvent();
    }
}
