.class Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity$3;
.super Ljava/lang/Object;
.source "PreferencesActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;->initColorPickerPreference()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;


# direct methods
.method constructor <init>(Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity$3;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v5, 0x0

    .line 107
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity$3;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 108
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity$3;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 109
    .local v1, "tv":Landroid/widget/TextView;
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setHorizontallyScrolling(Z)V

    .line 110
    const v2, 0x7f060038

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 111
    const-string v2, "ColorPicker"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "OK"

    new-instance v4, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity$3$1;

    invoke-direct {v4, p0}, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity$3$1;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity$3;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 116
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 117
    return v5
.end method
