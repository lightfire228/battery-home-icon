.class public Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;
.super Ljava/lang/Object;
.source "XposedMod.java"

# interfaces
.implements Lde/robv/android/xposed/IXposedHookLoadPackage;
.implements Lde/robv/android/xposed/IXposedHookInitPackageResources;


# instance fields
.field private final SYSTEMUI:Ljava/lang/String;

.field private final SYSTEMUI_STATUSBAR_PHONE:Ljava/lang/String;

.field private mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

.field private mBroadcastRegistered:Z

.field private mPrefs:Lde/robv/android/xposed/XSharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lde/robv/android/xposed/XSharedPreferences;

    const-string v1, "com.XtraordinairDEV.batteryhomebutton"

    invoke-direct {v0, v1}, Lde/robv/android/xposed/XSharedPreferences;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;

    .line 35
    const-string v0, "com.android.systemui"

    iput-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->SYSTEMUI:Ljava/lang/String;

    .line 36
    const-string v0, "com.android.systemui.statusbar.phone"

    iput-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->SYSTEMUI_STATUSBAR_PHONE:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;Landroid/widget/ImageView;)V
    .locals 0
    .param p0, "x0"    # Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->createBatteryIfNeeded(Landroid/widget/ImageView;)V

    return-void
.end method

.method static synthetic access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    .locals 1
    .param p0, "x0"    # Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Z
    .locals 1
    .param p0, "x0"    # Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBroadcastRegistered:Z

    return v0
.end method

.method static synthetic access$300(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->registerReceiverIfNeeded(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$400(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lde/robv/android/xposed/XSharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;

    return-object v0
.end method

.method static synthetic access$500(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)V
    .locals 0
    .param p0, "x0"    # Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->reloadSettings()V

    return-void
.end method

.method private createBatteryIfNeeded(Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "view"    # Landroid/widget/ImageView;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    if-nez v0, :cond_0

    .line 201
    new-instance v0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    invoke-direct {v0, p1}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    .line 202
    invoke-direct {p0}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->reloadSettings()V

    .line 203
    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->registerReceiverIfNeeded(Landroid/content/Context;)V

    .line 205
    :cond_0
    return-void
.end method

.method private registerReceiverIfNeeded(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 208
    iget-boolean v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBroadcastRegistered:Z

    if-eqz v1, :cond_0

    .line 262
    :goto_0
    return-void

    .line 211
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 212
    .local v0, "iF":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 213
    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 214
    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 215
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 216
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 217
    const-string v1, "com.XtraordinairDEV.batteryhomebutton.SETTINGS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 219
    new-instance v1, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;

    invoke-direct {v1, p0}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)V

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 261
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBroadcastRegistered:Z

    goto :goto_0
.end method

.method private reloadSettings()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/high16 v10, 0x42480000    # 50.0f

    .line 180
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;

    invoke-virtual {v7}, Lde/robv/android/xposed/XSharedPreferences;->reload()V

    .line 181
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;

    const-string v8, "padding"

    const v9, 0x3f333333    # 0.7f

    invoke-virtual {v7, v8, v9}, Lde/robv/android/xposed/XSharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v7

    mul-float/2addr v7, v10

    float-to-int v5, v7

    .line 182
    .local v5, "padding":I
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;

    const-string v8, "padding_landscape"

    const/high16 v9, 0x3f000000    # 0.5f

    invoke-virtual {v7, v8, v9}, Lde/robv/android/xposed/XSharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v7

    mul-float/2addr v7, v10

    float-to-int v4, v7

    .line 183
    .local v4, "lPadding":I
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;

    const-string v8, "stroke_width"

    const v9, 0x3dcccccd    # 0.1f

    invoke-virtual {v7, v8, v9}, Lde/robv/android/xposed/XSharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v7

    mul-float/2addr v7, v10

    float-to-int v6, v7

    .line 184
    .local v6, "width":I
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;

    const-string v8, "battery_charging_color"

    const v9, -0xd147d2

    invoke-virtual {v7, v8, v9}, Lde/robv/android/xposed/XSharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 185
    .local v0, "batteryChargingColor":I
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;

    const-string v8, "battery_low_color"

    const/high16 v9, -0x9a0000

    invoke-virtual {v7, v8, v9}, Lde/robv/android/xposed/XSharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 186
    .local v2, "batteryLowColor":I
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;

    const-string v8, "battery_default_color"

    const/4 v9, -0x1

    invoke-virtual {v7, v8, v9}, Lde/robv/android/xposed/XSharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 187
    .local v1, "batteryDefaultColor":I
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;

    const-string v8, "dynamic_colors"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lde/robv/android/xposed/XSharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 188
    .local v3, "colorIsDynamic":Z
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    iget-object v8, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;

    const-string v9, "charging_animation"

    invoke-virtual {v8, v9, v11}, Lde/robv/android/xposed/XSharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setChargingAnimationEnabled(Z)V

    .line 189
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    iget-object v8, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;

    const-string v9, "battery_percentage_size"

    const-string v10, "14"

    invoke-virtual {v8, v9, v10}, Lde/robv/android/xposed/XSharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setPercentageFontSize(I)V

    .line 190
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    iget-object v8, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;

    const-string v9, "battery_percentage"

    invoke-virtual {v8, v9, v11}, Lde/robv/android/xposed/XSharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setPercentageEnabled(Z)V

    .line 191
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    invoke-virtual {v7, v5, v4}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setPadding(II)V

    .line 192
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    invoke-virtual {v7, v6}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setStrokeWidth(I)V

    .line 193
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    invoke-virtual {v7, v1}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setBatteryDefaultColor(I)V

    .line 194
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    invoke-virtual {v7, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setBatteryLowColor(I)V

    .line 195
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    invoke-virtual {v7, v0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setBatteryChargingColor(I)V

    .line 196
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    invoke-virtual {v7, v3}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->updateColors(Z)V

    .line 197
    return-void
.end method


# virtual methods
.method public handleInitPackageResources(Lde/robv/android/xposed/callbacks/XC_InitPackageResources$InitPackageResourcesParam;)V
    .locals 8
    .param p1, "resparam"    # Lde/robv/android/xposed/callbacks/XC_InitPackageResources$InitPackageResourcesParam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 267
    const-string v4, "com.android.systemui"

    iget-object v5, p1, Lde/robv/android/xposed/callbacks/XC_InitPackageResources$InitPackageResourcesParam;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    iget-object v4, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;

    const-string v5, "hide_battery"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lde/robv/android/xposed/XSharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 271
    .local v1, "hideBattery":Z
    if-eqz v1, :cond_0

    .line 274
    const/4 v3, 0x0

    .line 276
    .local v3, "resId":I
    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "sony"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 277
    .local v2, "isSonyDevice":Z
    if-eqz v2, :cond_2

    .line 278
    iget-object v4, p1, Lde/robv/android/xposed/callbacks/XC_InitPackageResources$InitPackageResourcesParam;->res:Landroid/content/res/XResources;

    const-string v5, "msim_super_status_bar"

    const-string v6, "layout"

    const-string v7, "com.android.systemui"

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/XResources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 281
    :cond_2
    if-nez v3, :cond_3

    .line 282
    iget-object v4, p1, Lde/robv/android/xposed/callbacks/XC_InitPackageResources$InitPackageResourcesParam;->res:Landroid/content/res/XResources;

    const-string v5, "super_status_bar"

    const-string v6, "layout"

    const-string v7, "com.android.systemui"

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/XResources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 286
    :cond_3
    :try_start_0
    iget-object v4, p1, Lde/robv/android/xposed/callbacks/XC_InitPackageResources$InitPackageResourcesParam;->res:Landroid/content/res/XResources;

    new-instance v5, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$7;

    invoke-direct {v5, p0}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$7;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)V

    invoke-virtual {v4, v3, v5}, Landroid/content/res/XResources;->hookLayout(ILde/robv/android/xposed/callbacks/XC_LayoutInflated;)Lde/robv/android/xposed/callbacks/XC_LayoutInflated$Unhook;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 296
    :catch_0
    move-exception v0

    .line 297
    .local v0, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to hide battery: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lde/robv/android/xposed/XposedBridge;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleLoadPackage(Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;)V
    .locals 13
    .param p1, "lpparam"    # Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 40
    iget-object v8, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->packageName:Ljava/lang/String;

    const-string v9, "com.android.systemui"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    sget-object v8, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "lge"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 48
    .local v5, "isLGDevice":Z
    if-eqz v5, :cond_3

    .line 51
    :try_start_0
    const-string v8, "com.android.systemui.statusbar.phone.LGNavigationBarView"

    iget-object v9, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    .line 52
    invoke-static {v8, v9}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_0
    .catch Lde/robv/android/xposed/XposedHelpers$ClassNotFoundError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 67
    .local v1, "NavigationBarView":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_1
    new-instance v4, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$1;

    invoke-direct {v4, p0}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$1;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)V

    .line 108
    .local v4, "hook":Lde/robv/android/xposed/XC_MethodHook;
    const-string v8, "onFinishInflate"

    new-array v9, v12, [Ljava/lang/Object;

    aput-object v4, v9, v11

    invoke-static {v1, v8, v9}, Lde/robv/android/xposed/XposedHelpers;->findAndHookMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;

    .line 109
    const-string v8, "reorient"

    new-array v9, v12, [Ljava/lang/Object;

    aput-object v4, v9, v11

    invoke-static {v1, v8, v9}, Lde/robv/android/xposed/XposedHelpers;->findAndHookMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;

    .line 112
    if-nez v5, :cond_2

    .line 113
    const-string v8, "getIcons"

    new-array v9, v10, [Ljava/lang/Object;

    const-class v10, Landroid/content/res/Resources;

    aput-object v10, v9, v11

    new-instance v10, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$2;

    invoke-direct {v10, p0}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$2;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)V

    aput-object v10, v9, v12

    invoke-static {v1, v8, v9}, Lde/robv/android/xposed/XposedHelpers;->findAndHookMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;

    .line 126
    :cond_2
    new-instance v8, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$3;

    invoke-direct {v8, p0}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$3;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)V

    invoke-static {v1, v8}, Lde/robv/android/xposed/XposedBridge;->hookAllConstructors(Ljava/lang/Class;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 138
    if-eqz v5, :cond_0

    .line 139
    const-string v6, "com.lge.navigationbar"

    .line 142
    .local v6, "navBarLG":Ljava/lang/String;
    :try_start_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".HomeButton"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v8, v9}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    .line 143
    .local v0, "LGHomeButton":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-class v10, Landroid/content/Context;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-class v10, Landroid/util/AttributeSet;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    sget-object v10, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v10, v8, v9

    const/4 v9, 0x3

    sget-object v10, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v10, v8, v9

    const/4 v9, 0x4

    new-instance v10, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$4;

    invoke-direct {v10, p0}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$4;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)V

    aput-object v10, v8, v9

    invoke-static {v0, v8}, Lde/robv/android/xposed/XposedHelpers;->findAndHookConstructor(Ljava/lang/Class;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    .line 158
    .end local v0    # "LGHomeButton":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_2
    :try_start_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".NavigationThemeResource"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v8, v9}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    .line 159
    .local v2, "NavigationThemeResource":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v8, "getThemeResource"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-class v11, Landroid/view/View;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    new-instance v11, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$5;

    invoke-direct {v11, p0}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$5;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)V

    aput-object v11, v9, v10

    invoke-static {v2, v8, v9}, Lde/robv/android/xposed/XposedHelpers;->findAndHookMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 172
    .end local v2    # "NavigationThemeResource":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_0
    move-exception v7

    .line 173
    .local v7, "t":Ljava/lang/Throwable;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "BatteryHomeIcon: Failed to apply LG hook: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v7}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lde/robv/android/xposed/XposedBridge;->log(Ljava/lang/String;)V

    .line 174
    invoke-virtual {v7}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_0

    .line 54
    .end local v1    # "NavigationBarView":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "hook":Lde/robv/android/xposed/XC_MethodHook;
    .end local v6    # "navBarLG":Ljava/lang/String;
    .end local v7    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v3

    .line 55
    .local v3, "e":Lde/robv/android/xposed/XposedHelpers$ClassNotFoundError;
    const-string v8, "com.android.systemui.statusbar.phone.NavigationBarView"

    iget-object v9, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    .line 56
    invoke-static {v8, v9}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v1

    .line 59
    .restart local v1    # "NavigationBarView":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v5, 0x0

    .line 60
    goto/16 :goto_1

    .line 62
    .end local v1    # "NavigationBarView":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "e":Lde/robv/android/xposed/XposedHelpers$ClassNotFoundError;
    :cond_3
    const-string v8, "com.android.systemui.statusbar.phone.NavigationBarView"

    iget-object v9, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    .line 63
    invoke-static {v8, v9}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v1

    .restart local v1    # "NavigationBarView":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto/16 :goto_1

    .line 152
    .restart local v4    # "hook":Lde/robv/android/xposed/XC_MethodHook;
    .restart local v6    # "navBarLG":Ljava/lang/String;
    :catch_2
    move-exception v7

    .line 153
    .restart local v7    # "t":Ljava/lang/Throwable;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "BatteryHomeIcon: Failed to apply LG hook: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v7}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lde/robv/android/xposed/XposedBridge;->log(Ljava/lang/String;)V

    .line 154
    invoke-virtual {v7}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_2
.end method
