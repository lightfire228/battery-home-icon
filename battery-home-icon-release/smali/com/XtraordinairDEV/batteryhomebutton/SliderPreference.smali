.class public Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;
.super Landroid/preference/DialogPreference;
.source "SliderPreference.java"


# static fields
.field protected static final SEEKBAR_RESOLUTION:I = 0x19


# instance fields
.field protected mSeekBarValue:I

.field protected mSummaries:[Ljava/lang/CharSequence;

.field protected mValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    invoke-direct {p0, p1, p2}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->setup(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    invoke-direct {p0, p1, p2}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->setup(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;
    .param p1, "x1"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->getCurrentValueText(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getCurrentValueText(I)Ljava/lang/String;
    .locals 4
    .param p1, "value"    # I

    .prologue
    .line 156
    int-to-double v0, p1

    const-wide/high16 v2, 0x4039000000000000L    # 25.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setup(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    const v1, 0x7f030026

    invoke-virtual {p0, v1}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->setDialogLayoutResource(I)V

    .line 50
    sget-object v1, Lcom/XtraordinairDEV/batteryhomebutton/R$styleable;->SliderPreference:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 52
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->setSummary([Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 57
    return-void

    .line 53
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public getSummary()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 71
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mSummaries:[Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mSummaries:[Ljava/lang/CharSequence;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 72
    iget v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mValue:F

    iget-object v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mSummaries:[Ljava/lang/CharSequence;

    array-length v2, v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 73
    .local v0, "index":I
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mSummaries:[Ljava/lang/CharSequence;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 74
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mSummaries:[Ljava/lang/CharSequence;

    aget-object v1, v1, v0

    .line 76
    .end local v0    # "index":I
    :goto_0
    return-object v1

    :cond_0
    invoke-super {p0}, Landroid/preference/DialogPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mValue:F

    return v0
.end method

.method protected notifyChanged()V
    .locals 3

    .prologue
    .line 121
    invoke-super {p0}, Landroid/preference/DialogPreference;->notifyChanged()V

    .line 122
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.XtraordinairDEV.batteryhomebutton.SETTINGS_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 123
    return-void
.end method

.method protected onCreateDialogView()Landroid/view/View;
    .locals 5

    .prologue
    .line 127
    iget v3, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mValue:F

    const/high16 v4, 0x41c80000    # 25.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mSeekBarValue:I

    .line 128
    invoke-super {p0}, Landroid/preference/DialogPreference;->onCreateDialogView()Landroid/view/View;

    move-result-object v2

    .line 129
    .local v2, "view":Landroid/view/View;
    const v3, 0x102000b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 130
    .local v0, "currentValue":Landroid/widget/TextView;
    iget v3, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mSeekBarValue:I

    invoke-direct {p0, v3}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->getCurrentValueText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    const v3, 0x7f0d0064

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    .line 132
    .local v1, "seekbar":Landroid/widget/SeekBar;
    const/16 v3, 0x19

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 133
    iget v3, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mSeekBarValue:I

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 134
    new-instance v3, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference$1;

    invoke-direct {v3, p0, v0}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference$1;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;Landroid/widget/TextView;)V

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 152
    return-object v2
.end method

.method protected onDialogClosed(Z)V
    .locals 3
    .param p1, "positiveResult"    # Z

    .prologue
    .line 161
    iget v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mSeekBarValue:I

    int-to-float v1, v1

    const/high16 v2, 0x41c80000    # 25.0f

    div-float v0, v1, v2

    .line 162
    .local v0, "newValue":F
    if-eqz p1, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    invoke-virtual {p0, v0}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->setValue(F)V

    .line 165
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    .line 166
    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1
    .param p1, "a"    # Landroid/content/res/TypedArray;
    .param p2, "index"    # I

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 1
    .param p1, "restoreValue"    # Z
    .param p2, "defaultValue"    # Ljava/lang/Object;

    .prologue
    .line 66
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mValue:F

    invoke-virtual {p0, v0}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->getPersistedFloat(F)F

    move-result v0

    .end local p2    # "defaultValue":Ljava/lang/Object;
    :goto_0
    invoke-virtual {p0, v0}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->setValue(F)V

    .line 67
    return-void

    .line 66
    .restart local p2    # "defaultValue":Ljava/lang/Object;
    :cond_0
    check-cast p2, Ljava/lang/Float;

    .end local p2    # "defaultValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public setSummary(I)V
    .locals 2
    .param p1, "summaryResId"    # I

    .prologue
    .line 93
    :try_start_0
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->setSummary([Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :goto_0
    return-void

    .line 94
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Ljava/lang/Exception;
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->setSummary(I)V

    goto :goto_0
.end method

.method public setSummary(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "summary"    # Ljava/lang/CharSequence;

    .prologue
    .line 86
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mSummaries:[Ljava/lang/CharSequence;

    .line 88
    return-void
.end method

.method public setSummary([Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "summaries"    # [Ljava/lang/CharSequence;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mSummaries:[Ljava/lang/CharSequence;

    .line 82
    return-void
.end method

.method public setValue(F)V
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 104
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    .line 105
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->shouldPersist()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0, p1}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->persistFloat(F)Z

    .line 108
    :cond_0
    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mValue:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1

    .line 109
    iput p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->mValue:F

    .line 110
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/SliderPreference;->notifyChanged()V

    .line 112
    :cond_1
    return-void
.end method

.method protected shouldPersist()Z
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    return v0
.end method
