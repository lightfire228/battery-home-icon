.class public Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;
.super Landroid/preference/PreferenceActivity;
.source "PreferencesActivity.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# static fields
.field public static final INTENT_SETTINGS_CHANGED:Ljava/lang/String; = "com.XtraordinairDEV.batteryhomebutton.SETTINGS_CHANGED"

.field private static final URL_MohammadAG_APPS:Ljava/lang/String; = "market://search?q=pub:Mohammad Abu-Garbeyyeh"

.field private static final URL_MohammadAG_MODULES:Ljava/lang/String; = "http://repo.xposed.info/users/mohammadag"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private initColorPickerPreference()V
    .locals 2

    .prologue
    .line 103
    const-string v1, "color_picker_license"

    invoke-virtual {p0, v1}, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 104
    .local v0, "colorPickerPreference":Landroid/preference/Preference;
    new-instance v1, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity$3;

    invoke-direct {v1, p0}, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity$3;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 120
    return-void
.end method

.method private initMohammadAGPreference()V
    .locals 2

    .prologue
    .line 70
    const-string v1, "MohammadAGKey"

    invoke-virtual {p0, v1}, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 71
    .local v0, "mohammadAGPreference":Landroid/preference/Preference;
    new-instance v1, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity$2;

    invoke-direct {v1, p0}, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity$2;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 100
    return-void
.end method


# virtual methods
.method public getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WorldReadableFiles"
        }
    .end annotation

    .prologue
    .line 30
    const/4 v0, 0x1

    invoke-super {p0, p1, v0}, Landroid/preference/PreferenceActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 54
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 55
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 37
    const/high16 v0, 0x7f050000

    invoke-virtual {p0, v0}, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;->addPreferencesFromResource(I)V

    .line 39
    invoke-direct {p0}, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;->initMohammadAGPreference()V

    .line 40
    invoke-direct {p0}, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;->initColorPickerPreference()V

    .line 41
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 47
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 48
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/SharedPreferences;
    .param p2, "arg1"    # Ljava/lang/String;

    .prologue
    .line 59
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity$1;

    invoke-direct {v1, p0}, Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity$1;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/PreferencesActivity;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 66
    return-void
.end method
