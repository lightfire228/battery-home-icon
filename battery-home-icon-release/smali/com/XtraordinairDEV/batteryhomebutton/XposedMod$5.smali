.class Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$5;
.super Lde/robv/android/xposed/XC_MethodHook;
.source "XposedMod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->handleLoadPackage(Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;


# direct methods
.method constructor <init>(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)V
    .locals 0
    .param p1, "this$0"    # Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$5;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    invoke-direct {p0}, Lde/robv/android/xposed/XC_MethodHook;-><init>()V

    return-void
.end method


# virtual methods
.method protected afterHookedMethod(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)V
    .locals 3
    .param p1, "param"    # Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 163
    :try_start_0
    iget-object v1, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->args:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v1, v2

    check-cast v0, Landroid/widget/ImageView;

    .line 164
    .local v0, "v":Landroid/widget/ImageView;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "HomeButton"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$5;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # invokes: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->createBatteryIfNeeded(Landroid/widget/ImageView;)V
    invoke-static {v1, v0}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$000(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;Landroid/widget/ImageView;)V

    .line 166
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$5;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    invoke-virtual {p1, v1}, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->setResult(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    .end local v0    # "v":Landroid/widget/ImageView;
    :cond_0
    :goto_0
    return-void

    .line 168
    :catch_0
    move-exception v1

    goto :goto_0
.end method
